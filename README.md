# Phase refinery

An experimental tool for fast genotype population-based refinement and phasing.

Both refinement and phasing are performed with one joint model (matchng the HMM models of [SHAPEIT2](http://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1004234) and [Beagle](https://faculty.washington.edu/browning/beagle/beagle.html)). The main intended improvement is using [PBWT](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3998136/) population index (with my implementation in [rust-pbwt](https://github.com/gavento/rust-pbwt)) similarly as in the [Eagle2]() phasing algorithm, extending it to the refinement phase with appropriate inputs.

Contact [me](https://gavento.ucw.cz) if you are interested in any details, using the tool or cooperation on the project.

## Phasing

The proram is fully functional for fast phasing with a given panel (matching the basic features of Eagle2) and roughly matches the performance of Eagle2 (both speed an accuracy, depending on the parameters).

There is a variant using the read information for the phasing phase still in development.

## Refinement

The refinement part is also fully functional and performs reasonably well. It does not match SOTA yet but is much faster, reaching NRD=1.2 in 25 mins on 100k markers of human ch20 (where Beagle reaches NRD=0.9 in 1200 mins).

## Building

The tool is built in the [Rust language](https://www.rust-lang.org/en-US/). The best way to get rust is currently [rustup](https://rustup.rs/).

You can build the project by simply checking out this git repo and running `cargo build --release`. The binary is then located in `target/release/`. Run `phase --help` for compregensive usage overview.

