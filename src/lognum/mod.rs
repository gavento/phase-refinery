mod fixpoint;
mod float;

pub use self::fixpoint::{LogFixPointType, LogFixPoint};
pub use self::float::LogF64;