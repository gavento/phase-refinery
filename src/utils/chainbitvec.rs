use std::{
    ops::{Index, Range},
    iter::FromIterator,
    fmt,
    rc::Rc,
    sync::Arc,
    borrow::Borrow,
    };

/// Number of BitVecType-s in one ChainBitVec block
pub const CHAIN_BLOCK_SIZE: usize = 2;

type BitVecType = u64;
const CHUNK_SIZE: usize = 64;

#[derive(Clone, Debug)]
pub struct ChainBitVec {
    last: [BitVecType; CHAIN_BLOCK_SIZE],
    last_len: usize,
    prev_len: usize,
    prev: Option<Arc<ChainBitVec>>,
}

impl ChainBitVec {
    /// Create a new empty vector
    pub fn new() -> Self {
        ChainBitVec {
            last: [0; CHAIN_BLOCK_SIZE],
            last_len: 0,
            prev_len: 0,
            prev: None,
        }
    }

    /// Return the vector length in bits
    #[inline]
    pub fn len(&self) -> usize {
        self.prev_len + self.last_len
    }

    /// Get a bit value by a bit-index
    #[inline]
    pub fn get(&self, index: usize) -> bool {
        if index >= self.len() {
            panic!("ChainBitVec::get index out of bounds: {} of {}", index, self.len());
        }
        if index < self.prev_len {
            self.prev.as_ref().expect("Bug in ChainedBitVec!").get(index)
        } else {
            let last_index = index - self.prev_len;
            (self.last[last_index / CHUNK_SIZE] & (1 << (last_index % CHUNK_SIZE))) != 0
        }
    }

    /// Get a bit value by a bit-index indexed from the end (0 is the last element)
    #[inline]
    pub fn get_end(&self, back_index: usize) -> bool {
        if back_index >= self.len() {
            panic!("ChainBitVec::get_end index out of bounds: {} of {}", back_index, self.len());
        }
        self.get(self.len() - back_index - 1)
    }

    /// Private set in the last block without checking the bit-index bounds (still memory safe)
    #[inline]
    fn priv_set(&mut self, last_index: usize, val: bool) {
        if val {
            self.last[last_index / CHUNK_SIZE] |= 1 << (last_index % CHUNK_SIZE);
        } else {
            self.last[last_index / CHUNK_SIZE] &= !(1 << (last_index % CHUNK_SIZE));
        }
    }

    /// Extend the vector with a single value
    pub fn push(&mut self, val: bool) {
        if self.last_len == CHAIN_BLOCK_SIZE * CHUNK_SIZE {
            let prev = Arc::new(self.clone());
            self.last = [0; CHAIN_BLOCK_SIZE];
            self.last_len = 0;
            self.prev_len = prev.prev_len + prev.last_len;
            self.prev = Some(prev);
        }
        let i = self.last_len;
        self.priv_set(i, val);
        self.last_len += 1;
    }

    pub fn iter<'a>(&'a self) -> ChainBitVecIter<'a> {
        let mut v = ChainBitVecIter { offset: 0, bitvecs: Vec::new() };
        let mut ch = self;
        loop {
            if ch.last_len > 0 {
                v.bitvecs.push((ch.last_len, &ch.last));
            }
            if let Some(ref prev) = ch.prev {
                ch = prev.borrow();
            } else {
                break;
            }
        }
        v
    }
    
    #[allow(dead_code)]
    pub(super) fn slow_eq(&self, other: &Self) -> bool {
        if self.len() != other.len() {
            return false;
        }
        for i in 0..self.len() {
            if self.get(i) != other.get(i) { return false; }
        }
        true
    }

    /// Create a ChainBitVec from a slice (indexed from the left, same as `get`).
    /// TODO: Slower (iterated).
    pub fn slice(&self, range: Range<usize>) -> ChainBitVec {
        ChainBitVec::from_iter(self.iter().skip(range.start).take(range.end - range.start))
    }
}

impl Index<usize> for ChainBitVec {
    type Output = bool;

    fn index(&self, index: usize) -> &bool {
        static ST: bool = true;
        static SF: bool = false;
        if self.get(index) { &ST } else { &SF }
    }
}

impl FromIterator<bool> for ChainBitVec {
    fn from_iter<T: IntoIterator<Item = bool>>(iter: T) -> Self {
        let mut bv = ChainBitVec::new();
        for i in iter {
            bv.push(i);
        }
        bv
    }
}

impl<'a> FromIterator<&'a bool> for ChainBitVec {
    fn from_iter<T: IntoIterator<Item = &'a bool>>(iter: T) -> Self {
        let mut bv = ChainBitVec::new();
        for i in iter {
            bv.push(*i);
        }
        bv
    }
}

impl Eq for ChainBitVec {}

impl PartialEq for ChainBitVec {
    fn eq(&self, other: &Self) -> bool {
        self.iter().eq(other.iter())
    }
}

#[derive(Clone, Debug)]
pub struct ChainBitVecIter<'a> {
    // offset in the last of bitvecs
    offset: usize,
    bitvecs: Vec<(usize, &'a [BitVecType])>,
}

impl<'a> Iterator for ChainBitVecIter<'a> {
    type Item = bool;
    fn next(&mut self) -> Option<Self::Item> {
        let mut v = None;
        if let Some((l, ch)) = self.bitvecs.pop() {
            v = Some(ch[self.offset / CHUNK_SIZE] & (1 << (self.offset % CHUNK_SIZE)) != 0);
            self.offset += 1;
            if self.offset < l {
                self.bitvecs.push((l, ch));
            } else {
                self.offset = 0;
            }
        }
        v
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter;

    #[test]
    fn test_base() {
        let mut v = ChainBitVec::new();
        assert_eq!(v.len(), 0);
        v.push(true);
        v.push(true);
        v.push(false);
        v.push(true);
        assert_eq!(v.len(), 4);
        assert_eq!(v.get(0), true);
        assert_eq!(v.get(2), false);
        assert_eq!(v[2], false);
        assert_eq!(v.iter().collect::<Vec<bool>>(),
                   vec![true, true, false, true]);
        let v2 = ChainBitVec::from_iter(&[true, true, false, true]);
        assert_eq!(v, v2);
    }

    #[test]
    fn test_long() {
        let mut v = ChainBitVec::from_iter(iter::repeat(true).take(2000));
        assert_eq!(v.len(), 2000);
        v.push(false);
        v.push(false);
        assert_eq!(v.len(), 2002);
        assert_eq!(v.get(0), true);
        assert_eq!(v.get(2001), false);
        let mut v2 = v.clone();
        v2.push(true);
        assert_eq!(v.len(), 2002);
        assert_eq!(v2.len(), 2003);
        let mut vcol = v2.iter().collect::<Vec<bool>>();
        assert!(vcol.iter().map(|b| *b).ne(v.iter()));
        vcol.pop();
        assert!(vcol.iter().map(|b| *b).eq(v.iter()));
        v.push(true);
        assert_eq!(v, v2);
    }
}
