use itertools::Itertools;
use std::vec::Vec;
use std::cmp::max;
use rayon::prelude::*;
use std::fmt::Write;

#[allow(unused_imports)]
use ::*;
use super::*;
use utils::{ref_vec, map01, SimpleHash, ConsPairsIter, WeightedStat, max2, min2};

pub const MIN_CROSSOVER_L: Likelyhood = Likelyhood::from_log10_milis(-6000);

#[derive(Debug, PartialEq)]//, Serialize, Deserialize)]
pub struct State {
    pub hist: GenoPair,
    refs: [RefSet; 2],
    pub cand: GenoPair,
    state_l: Likelyhood,
    cand_l: Likelyhood,
}

impl State {
    pub fn empty(refs: RefSet) -> Self {
        State {
            hist: GenoPair::new(),
            refs: [refs.clone(), refs],
            state_l: Likelyhood::from_float(1.0),
            cand_l: Likelyhood::from_float(1.0),
            cand: GenoPair::new(),
        }
    }
}

impl Clone for State {
    fn clone(&self) -> Self {
        State {
            hist: self.hist.clone(),
            refs: map01(|i| self.refs[i].clone()),
            cand: self.cand.clone(),
            state_l: self.state_l,
            cand_l: self.cand_l,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Computation<'a> {
    panel: &'a RefPanel,
    states: Vec<State>,
    pub pos: LocIndex,
    pub sample: &'a VariantSample,
    gene_map: &'a GeneMap,
    cfg: ComputationCfg,
}

#[derive(Debug, Clone, Default)]
pub struct ComputationCfg {
    pub drop_states: Option<usize>,
    pub merge_states: usize,
    pub new_var_prob: Likelyhood,
    pub crossover_prob_mult: Likelyhood,
    pub use_state_likelihood: bool,
}


//const MIN_VARSAMP_PROB: f64 = 1e-10;

use std::cmp::{Ordering, PartialOrd};
use std::iter::Iterator;

impl<'a> Computation<'a> {
    pub fn new(panel: &'a RefPanel,
               sample: &'a VariantSample,
               gene_map: &'a GeneMap,
               cfg: ComputationCfg) -> Self {
        Computation {
            panel: panel,
            states: vec![State::empty(RefSet::for_panel(panel))],
            pos: 0,
            sample: sample,
            gene_map: gene_map,
            cfg: cfg,
        }
    }

    fn best_state_by<T: PartialOrd, F: Fn(&State) -> T>(&'a self, f: F) -> Option<&'a State> {
        self.states.iter().max_by(
            |a, b| PartialOrd::partial_cmp(&f(&a), &f(&b))
                .expect("Invalid float values to compare.")
        )
    }

    /// Return the state with the highest `cand_l`.
    pub fn best_candidate(&'a self) -> Option<&'a State> {
        self.best_state_by(|s| s.cand_l)
    }

    /// Return the state with highest `state_l`.
    pub fn best_state(&'a self) -> Option<&'a State> {
        self.best_state_by(|s| s.state_l)
    }

    /// A wrapper for extending a refset within the current panel
    #[inline]
    pub fn extend_refset(&self, ref_set: &RefSet, var: Variant) -> RefSet {
        ref_set.extend(var, self.pos, self.panel)
    }

    pub fn extend_state(&self, s: &State, target: &mut Vec<State>) {
        let refs0_ext = map01(|i| self.extend_refset(&s.refs[0], i as i8));
        let refs1_ext = map01(|i| self.extend_refset(&s.refs[1], i as i8));
        for gt0 in 0..2 {
            for gt1 in 0..2 {
                let nrefs = [refs0_ext[gt0].clone(), refs1_ext[gt1].clone()];
                if nrefs[0].is_empty() || nrefs[1].is_empty() {
                    continue;
                }
                let penalty = (self.sample.probs[self.pos as usize][gt0 + gt1] +
                    self.cfg.new_var_prob) / (self.cfg.new_var_prob * 3.0 + 1.0);
                let refs_penalty = Likelyhood::from_float(
                    nrefs[0].len() as f64 * nrefs[1].len() as f64 /
                    s.refs[0].len() as f64 / s.refs[1].len() as f64);
                target.push(State {
                     hist: s.hist.extend([gt0 as i8, gt1 as i8]),
                     refs: nrefs,
                     state_l: s.state_l * penalty * refs_penalty,
                     cand_l: s.cand_l * penalty * refs_penalty,
                     cand: s.cand.extend([gt0 as i8, gt1 as i8]),
                })
            }
        }
    }

    /// Create a `State` where one or both histories are shortened to 0
    pub fn crossover_state(&self, states: Vec<&State>, erase_hist: [bool; 2]) -> State {
        assert!(states.len() > 0);
        assert!(erase_hist[0] || erase_hist[1]);
        let s0 = states[0];
        let cand_tuples: Vec<(&GenoPair, Likelyhood)> = states.iter().map(|s| (&s.cand, s.cand_l)).collect();
        let (ncand_gp, ncand_l) = Self::merge_candidates(cand_tuples);
        let mut penalty = if self.pos + 1 < self.panel.M() {
            self.cfg.crossover_prob_mult * (
            self.gene_map.get_gmap(self.sample.positions[(self.pos + 1) as usize]) -
            self.gene_map.get_gmap(self.sample.positions[self.pos as usize]))
        } else { Likelyhood::from_log10(-1000.0) }; // No crossover prob. in the last step
        if penalty < MIN_CROSSOVER_L { penalty = MIN_CROSSOVER_L; }
        if erase_hist[0] && erase_hist[1] { penalty = penalty * penalty; }
        State {
            hist: s0.hist.erase(erase_hist),
            refs: map01(|i| if erase_hist[i] {
                RefSet::for_panel(self.panel)
            } else {
                s0.refs[i].clone()
            }),
            state_l: states.iter().map(|s| s.state_l).sum::<Likelyhood>() * penalty,
            cand_l: ncand_l * penalty,
            cand: ncand_gp.clone(),
        }
    }

    pub fn merge_candidates<'b>(mut cands: Vec<(&'b GenoPair, Likelyhood)>) -> (&'b GenoPair,
                                                                              Likelyhood) {
        cands.sort_by_key(|c| c.0.hash());
        let mut max_gp: &'b GenoPair = cands[0].0;
        let mut max_l: Likelyhood = Likelyhood::zero();
        let mut sum_l: Likelyhood = Likelyhood::zero();

        for (cur, next) in ConsPairsIter::new(cands.iter()) {
            let (gp, l) = *cur;
            if next.is_none() || gp.hash() != next.unwrap().0.hash() {
                if sum_l > max_l {
                    max_l = sum_l;
                    max_gp = gp;
                }
                sum_l = Likelyhood::zero();
            } else {
                sum_l = sum_l + l;
            }
        }
        (max_gp, max_l)
    }

    pub fn show_tree(&self) {
        let mut tv = ref_vec(&self.states);
        tv.sort_by(|sa, sb| Ord::cmp(&sa.hist, &sb.hist));
        for s in tv {
            println!("{: >20} {:.2} {:.2}", format!("{}", s.hist), s.state_l, s.cand_l);
        }
    }

    pub fn state_stats(&self) -> String {
        let mut s = String::new();
        let lmult = self.states[0].state_l;
        let mut max_len_w = WeightedStat::new();
        let mut min_len_w = WeightedStat::new();
        let mut len_diff_w = WeightedStat::new();
        let mut max_len = WeightedStat::new();
        let mut min_len = WeightedStat::new();
        let mut len_diff = WeightedStat::new();
        for s in self.states.iter() {
            let sw = (s.state_l / lmult).to_float();
            let maxlen = max2(&s.hist.lengths()) as f64;
            let minlen = min2(&s.hist.lengths()) as f64;
            max_len.add(maxlen, 1.0);
            min_len.add(minlen, 1.0);
            len_diff.add(maxlen - minlen, 1.0);
            max_len_w.add(maxlen, sw);
            min_len_w.add(minlen, sw);
            len_diff_w.add(maxlen - minlen, sw);
        }
        writeln!(&mut s, "max_len:  {} weighted: {}", max_len, max_len_w).unwrap();
        writeln!(&mut s, "min_len:  {} weighted: {}", min_len, min_len_w).unwrap();
        writeln!(&mut s, "len_diff: {} weighted: {}", len_diff, len_diff_w).unwrap();
        writeln!(&mut s, "state entropy: {}", len_diff_w.entropy());
        s
    }

    pub fn step(&mut self) {
        /*let mut exp_states = //Vec::new();
        {
            let mut m = std::sync::Mutex::new(Vec::new());
            self.states.par_iter().for_each(|s| {
                let mut res = Vec::with_capacity(4);
                self.extend_state(s, &mut res);
                m.lock().unwrap().append(&mut res);
            });
            m.into_inner().unwrap()
        };*/
        let mut exp_states = Vec::with_capacity(self.states.len() * 5);
        for s in self.states.iter() {
            self.extend_state(s, &mut exp_states);
        }

        if exp_states.len() == 0 {
            println!("{:#?}", self.sample.probs[self.pos as usize]);
            let l = self.states.last().unwrap();
            println!("{:#?}", l);
            let mut ns = Vec::new();
            self.extend_state(&l, &mut ns);
            println!("{:#?}", ns);

        }
        assert!(exp_states.len() > 0);

        let mut new_states = Vec::with_capacity(self.states.len() * 3);
        for i in 0..2 {
            let mut collapse_i: Vec<&State> = ref_vec(&exp_states);
            collapse_i.sort_by_key(|s: &&State| s.hist.hashes[1 - i]);
            for (_, group) in collapse_i
                    .into_iter()
                    .group_by(|s: &&State| s.hist.hashes[1 - i])
                    .into_iter() {
                new_states.push(self.crossover_state(group.collect_vec(), [i == 0, i == 1]));
            }
        }

        new_states.push(self.crossover_state(ref_vec(&exp_states), [true, true]));
        exp_states.append(&mut new_states);
        self.states = exp_states;

        if let Some(ms) = self.cfg.drop_states.clone() {
            if self.cfg.use_state_likelihood {
                self.filter_states_by_state_l(ms);
            } else {
                self.filter_states_by_cand_l(ms);
            }
        }

        // TODO: Merge states

        self.pos += 1;
    }

    pub fn run(&mut self) {
        while self.pos < self.panel.M() {
            self.step();
            if self.pos % 1000 == 0 {
                info!("State stats at {}:\n{}", self.pos, self.state_stats());
            }
            debug!("Step {}, {} states", self.pos, self.states.len())
        }
    }

    fn filter_states_by_key<F: Fn(&&State) -> Likelyhood>(&mut self, f: F, limit: usize) {
        if self.states.len() <= limit { return; }
        let threshold = {
            let mut sref = ref_vec(&self.states);
            sref.sort_by_key(&f);
            f(&sref[sref.len() - 1 - limit])
        };
        self.states.retain(|s| f(&s) > threshold);
        assert!(self.states.len() <= limit);
        if limit > 0 { assert!(self.states.len() > 0); }
    }

    pub fn filter_states_by_cand_l(&mut self, limit: usize) {
        self.filter_states_by_key(|s| s.cand_l, limit);
    }

    pub fn filter_states_by_state_l(&mut self, limit: usize) {
        self.filter_states_by_key(|s| s.state_l, limit);
    }

}



#[cfg(test)]
mod tests {
    use super::*;
    use bitvec::BitVec;
    use std::iter::FromIterator;

    lazy_static! {
        static ref TEST_GENOTYPES: Vec<BitVec> =
            [
                &[0, 1, 0],
                &[0, 0, 0],
                &[0, 0, 1],
                &[0, 1, 1],
                &[0, 0, 1],
            ].iter().map(|v| v.iter().map(|i| *i == 1u8).collect()).collect();
        static ref TEST_LOCS: Vec<Position> =
            vec![2, 4, 6, 8, 10];
        static ref TEST_PANEL: RefPanel = RefPanel {
            locs: TEST_LOCS.clone(),
            sample_names: vec!["A".into(), "B".into(), "C".into()],
            genotypes: TEST_GENOTYPES.clone(),
            suff_index: None,
        };

        static ref TEST_VARSAMPLE :VariantSample = VariantSample {
            sample_name: "S007".into(),
            positions: TEST_LOCS.clone(),
            alleles: Vec::new(),
            probs: [
                [0.333, 0.333, 0.333],
                [0.333, 0.333, 0.333],
                [0.3, 1e-12, 0.7],
                [1e-12, 0.1, 0.9],
                [0.333, 0.333, 0.333],
                ].iter().map(|lprobs: &[f64; 3]| lprobs.iter().map(
                    |p| Likelyhood::from_float(*p))
                .collect())
                .collect(),
        };
        static ref TEST_GENEMAP :GeneMap = GeneMap {
            name: "GM1".into(),
            locs: vec![0, 10],
            rate: vec![0.1, 0.2],
            gmap: vec![0.0, 1.5e-6],
        };
    }
/*
    #[test]
    fn test_computation() {
        let mut c = Computation::new(&TEST_PANEL, &TEST_VARSAMPLE, &TEST_GENEMAP);
        assert_eq!(c.states.len(), 1);
        c.step();
        assert_eq!(c.states.len(), 4 + 5); // Hand verified
        c.step();
        assert_eq!(c.states.len(), 9 + 7); // Hand verified
        c.step();
        assert_eq!(c.states.len(), 40); // Not hand-verified
        c.step();
        assert_eq!(c.states.len(), 92); // Not hand-verified
        c.filter_states_by_cand_l(30);
        assert_eq!(c.states.len(), 30); // Not hand-verified
        c.filter_states_by_state_l(20);
        assert_eq!(c.states.len(), 20); // Not hand-verified
    }*/
/*
    #[test]
    fn test_state_extend() {
        let panel = TEST_PANEL.clone();
        let l1 = Likelyhood::from_prob(1.0);
        let state = State::empty(panel.ref_set());
        let state_01 = state.extend([0, 1], 0, &panel, l1).unwrap();
        let state_10 = state.extend([1, 0], 0, &panel, l1).unwrap();
        assert_eq!(state_01.state_l, state_10.state_l);
        assert_eq!(state_01.extend([1, 0], 1, &panel, l1), None);
        let state_01_00 = state_01.extend([0, 0], 1, &panel, l1).unwrap();
        assert_eq!(format!("{}", state_01_00.cand), "01,00");
    }*/
  /*      #[test]#[allow(non_snake_case)]
    fn test_ht_merge() {
        let htA = HaploType::empty().extend([0, 1]);
        let htB = HaploType::empty().extend([1, 0]);
        let htC = HaploType::empty().extend([1, 1]);
        let l0 = Likelyhood::from_prob(0.0);
        let l1 = Likelyhood::from_prob(1.0);
        let l2 = Likelyhood::from_prob(2.0);
        let seq1 = vec![(&htC, l2), (&htB, l1), (&htA, l0), (&htB, l1), (&htA, l0), (&htB, l1)];
        let (seq1_ht, seq1_l) = HaploType::merge_candidates(seq1);
        assert_eq!(seq1_ht, &htB);
        assert_eq!(seq1_l.to_prob(), 3.0);
    }
*/
}
