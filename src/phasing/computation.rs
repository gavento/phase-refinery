#![allow(dead_code)]
#![allow(unused_imports)]

use itertools::Itertools;
use std::vec::Vec;
use std::cmp::max;
use rayon::prelude::*;
use std::fmt::Write;
use std::cmp::{Ordering, PartialOrd};
use std::iter::Iterator;
use pbwt::*;
use std::collections::HashMap;
use rust_htslib::bcf::record::GenotypeAllele;
use std::iter::FromIterator;
use std::mem;
use std::path::Path;
use super::super::formats::write_candidates_vcf;

use super::super::*;
use super::*;
use crate::utils::*;

pub const MIN_CROSSOVER_L: f64 = 1e-6;

#[derive(Debug)]
pub struct Computation {
    /// Samples used as the panel (assumed phased)
    pub panel: SampleSet,
    /// PBWT structure over `panel`
    pbwt: PBWTState,
    /// Genetic map for crossover probabilities
    gene_map: GeneMap,
    /// Samples to be phased
    pub sampleset: SampleSet,
    /// State sets for every sample from the above set
    pub samples: Vec<Sample>,
    /// Computation configuration
    pub cfg: ComputationCfg,
    /// Variants that are net phased by us, indexed by index in sample set
    pub unphased: HashMap<Index, Vec<GenotypeAllele>>,
}

#[derive(Debug, Clone)]
pub struct Sample {
    pub states: Vec<State>,
    pub name: String,
}

#[derive(Debug, Clone, Default)]
pub struct ComputationCfg {
    /// Number of states to keep after pruning
    pub limit_states: usize,
    /// Minimal state likelihood fraction to keep
    pub state_merge_fraction: Option<Likelyhood>,
    /// Unused
    pub new_var_prob: Likelyhood,
    /// Mean IBD length (`a` in the Eagle paper), Morgans (*NOT* cM)
    pub mean_ibd: f64,
    /// Use state likelihood to select the best candidate (only in the last step)
    pub use_state_likelihood: bool,
    /// Do only the phasing step, expecting GT rather than PL vcf data
    pub phase_only: bool,
}

impl Computation {
    pub fn new(panel: SampleSet,
               sampleset: SampleSet,
               gene_map: GeneMap,
               cfg: ComputationCfg) -> Self {
        let pbwt = PBWTState::new(panel.m() * 2);
        let ref0 = PBWTCursor::new_full_range(&pbwt);
        Computation {
            panel: panel,
            pbwt: pbwt,
            gene_map: gene_map,
            samples: sampleset.sample_names
                              .iter()
                              .map(|name| 
                                    Sample { 
                                        states: vec![State::new(ref0.clone())],
                                        name: name.clone()
                                    })
                              .collect(),
            sampleset: sampleset,
            cfg: cfg,
            unphased: HashMap::new(),
        }
    }

    fn best_states_by<'a, T: PartialOrd, F: Fn(&State) -> T>(&'a self, f: F) ->
            Vec<&'a State> {
        self.samples.iter().map(|sample|
            sample.states.iter().max_by(
                |a, b| PartialOrd::partial_cmp(&f(&a), &f(&b))
                    .expect("Invalid float values to compare.")
            ).expect("a sample with no states")
        ).collect()
    }
 
    /// Return the states with the highest `cand_l`.
    pub fn best_candidates<'a>(&'a self) -> Vec<&'a State> {
        self.best_states_by(|s| s.cand_l)
    }

    /// Return the states with highest `state_l`.
    pub fn best_states<'a>(&'a self) -> Vec<&'a State> {
        self.best_states_by(|s| s.state_l)
    }

    pub fn step(&mut self) -> Result<bool> {
        let mut sample_r = None;
        let mut panel_r = None;
        while sample_r.is_none() || panel_r.is_none() {
            if panel_r.is_none() {
                panel_r = match (&mut self.panel).next() {
                    Some(rec) => Some(rec?),
                    _ => return Ok(false)
                };
            }
            if sample_r.is_none() {
                sample_r = match (&mut self.sampleset).next() {
                    Some(rec) => Some(rec?),
                    _ => return Ok(false)
                };
            }
            let panel_pos = panel_r.as_ref().unwrap().pos;
            let panel_num = panel_r.as_ref().unwrap().num;
            let sample_pos = sample_r.as_ref().unwrap().pos;
            let sample_num = sample_r.as_ref().unwrap().num;
            if self.panel.alleles[panel_num as usize].len() != 2 {
                // Multi-allelic site in panel
                // ignore it
                panel_r = None;
                continue;
            }

            let mut singleton_panel = false;
            if let Some(ref pr) = panel_r {
                let mut cnt = [0usize, 0];
                for g in &pr.gt { match g {
                    &GenotypeAllele::Phased(v) => cnt[v as usize] += 1,
                    _ => (),
                }};
                drop(pr);
                if cnt[0] < 1 || cnt[1] < 1 {
                    singleton_panel = true;
                    debug!("Panel position {} (index {}) has value count {:?}, skipped", sample_pos, sample_num, cnt);
                }
            }
            if singleton_panel {
                // Panel position only has one allelle
                panel_r = None;
                continue;
            }

            if panel_pos < sample_pos {
                // A position in panel not in the sample
                // ignore it
                panel_r = None;
                continue;
            }
            if self.sampleset.alleles[sample_num as usize].len() != 2 {
                // Multi-allelic site in samples
                // output it unphased
                let sr = sample_r.take().unwrap();
                debug!("Multi-allelic sample at position {} (index {}) skipped", sample_pos, sample_num);
                if self.cfg.phase_only {
                    self.unphased.insert(sample_num, sr.gt);
                } else {
                    self.unphased.insert(sample_num, sr.gt.iter().map(|_| GenotypeAllele::UnphasedMissing).collect());
                }
                continue;
            }
            if panel_pos > sample_pos {
                // A position in the sample not in the panel
                // output it unphased
                let sr = sample_r.take().unwrap();
                debug!("Sample position {} (index {}) not in panel, skipped", sample_pos, sample_num);
                if self.cfg.phase_only {
                    self.unphased.insert(sample_num, sr.gt);
                } else {
                    self.unphased.insert(sample_num, sr.gt.iter().map(|_| GenotypeAllele::UnphasedMissing).collect());
                }
                continue;
            }
        }
        // Here we have matching positions, bi-allelic variants
        let panel_r = panel_r.unwrap();
        let sample_r = sample_r.unwrap();

        // Advance PBWT state
        let next_col = BitVec::from_iter(panel_r.gt.iter().map(
            |gt| match *gt {
                GenotypeAllele::Phased(i) => i != 0,
                _ => panic!("unphased or missing allele in panel at pos {}", panel_r.pos),
            }
        ));
        self.pbwt.set_next_column(next_col);

        // Update states
        let mut samples = Vec::new();
        mem::swap(&mut samples, &mut self.samples);
        samples.par_iter_mut().enumerate().for_each(|(smp_i, smp)| {
            if self.cfg.phase_only {
                let gtpair = map01(|i| match sample_r.gt[smp_i * 2 + i] {
                    GenotypeAllele::Phased(_) => panic!("phased alleles in samples no impl yet"),
                    GenotypeAllele::Unphased(gt) => gt as Variant,
                    _ => panic!("missing allele in sample while phasing"),
                });
                self.step_for_sample(smp, sample_r.pos, sample_r.num, Some(gtpair), None);
            } else {
                let pl = &sample_r.probs[smp_i];
                self.step_for_sample(smp, sample_r.pos, sample_r.num, None, Some(pl));
            }
        });
        mem::swap(&mut samples, &mut self.samples);

        // Advance PBWT state
        self.pbwt = self.pbwt.advance();

        Ok(true)
    }

    fn step_for_sample(&self, sample: &mut Sample, pos: Index, num: Index, gt: Option<VariantPair>, pl: Option<&[Likelyhood]>) {
        assert_eq!(gt.is_none(), pl.is_some());

        let mut cross_states: Vec<State> = sample.states.clone();

        // Only crossover for states other than first
        if num > 0 {
            // Add one-parent crossover states
            for i in 0..2 {
                let mut collapse_i: Vec<&State> = ref_vec(&sample.states);
                collapse_i.sort_by_key(|s: &&State| s.hist.hashes[1 - i]);
                for (_, group) in collapse_i
                        .into_iter()
                        .group_by(|s: &&State| s.hist.hashes[1 - i])
                        .into_iter() {
                    cross_states.push(self.crossover_state(pos, num, group.collect(), [i == 0, i == 1]));
                }
            }

            // Add both-parents crossover state
            cross_states.push(self.crossover_state(pos, num, sample.states.iter().collect(), [true, true]));
        }

        let mut ext_states: Vec<State> = Vec::new();

        // Add state extensions
        for s in cross_states {
            match (gt, pl) {
                (Some(gti), None) => s.push_extension_with(&mut ext_states, &self.pbwt, gti),
                (None, Some(pli)) => s.push_extension_pl(&mut ext_states, &self.pbwt, pli),
                _ => unreachable!()
            }
        }
        assert!(ext_states[0].cand.len() > num as usize / 2);
        let statlen0 = ext_states.len();

        // Merge states with likelihood below state_min_likelihood
        let (sm_num, sm_frac) = self.merge_unlikely_states(&mut ext_states);
        let statlen1 = ext_states.len();
        let statlik1 = ext_states.iter().map(|s| s.state_l).sum::<Likelyhood>();

        // Limit the number of retained states
        ext_states.sort_unstable_by_key(|s| s.state_l);
        let nsl = ext_states.len();
        if nsl > self.cfg.limit_states {
            ext_states.drain(..(nsl - self.cfg.limit_states));
        }
        let statlen2 = ext_states.len();
        let statlik2 = ext_states.iter().map(|s| s.state_l).sum::<Likelyhood>();
        
        info!("Expanded to {} states. Merged {} states to {} ({} likelihood fraction). Dropped {} states ({} likelihood fraction)",
            statlen0, sm_num, statlen1 - (statlen0 - sm_num), sm_frac, statlen1 - statlen2, (1.0 - (statlik2 / statlik1).to_float()));
        sample.states = ext_states;
    }

    /// Create a new `State` where one or both histories are shortened to 0
    pub fn crossover_state(&self, pos: Index, num: Index, states: Vec<&State>, erase_hist: [bool; 2]) -> State {
        assert!(states.len() > 0);
        assert_eq!(self.sampleset.positions[num as usize], pos);
        assert!(erase_hist[0] || erase_hist[1]);
        let s0 = states[0];
        let cand_tuples: Vec<(&GenoPair, Likelyhood)> = states.iter().map(|s| (&s.cand, s.cand_l)).collect();
        let (ncand_gp, ncand_l) = Self::merge_candidates(cand_tuples);
        // Genetic distance to the last location in Morgans
        let gen_len = self.gene_map.get_gmap(pos) - self.gene_map.get_gmap(self.sampleset.positions[num as usize - 1]);
        assert!(gen_len >= 0.0);
        let penalties = map01(|i| if !erase_hist[i] { 1.0 } else { 
            let mut p: f64 = 1.0 - (- gen_len / self.cfg.mean_ibd).exp();
            if p < MIN_CROSSOVER_L { p = MIN_CROSSOVER_L; }
            //info!("GL {}, IBD {}, p={}", gen_len, self.cfg.mean_ibd, p);
            p
        });
        State {
            hist: s0.hist.erase(erase_hist),
            refs: map01(|i| if erase_hist[i] {
                PBWTCursor::new_full_range(&self.pbwt)
            } else {
                s0.refs[i].clone()
            }),
            state_l: states.iter().map(|s| s.state_l).sum::<Likelyhood>() * penalties[0] * penalties[1],
            cand_l: ncand_l * penalties[0] * penalties[1],
            cand: ncand_gp.clone(),
        }
    }

    /// Select the candidate GP with the highest likelyhood,
    /// summing probabilities in case the GP is present multiple times.
    pub fn merge_candidates<'b>(mut cands: Vec<(&'b GenoPair, Likelyhood)>) ->
            (&'b GenoPair, Likelyhood) {
        cands.sort_by_key(|c| c.0.hash());
        let mut max_gp: &'b GenoPair = cands[0].0;
        let mut max_l: Likelyhood = Likelyhood::zero();
        let mut sum_l: Likelyhood = Likelyhood::zero();

        for (cur, next) in ConsPairsIter::new(cands.iter()) {
            let (gp, l) = *cur;
            if next.is_none() || gp.hash() != next.unwrap().0.hash() {
                if sum_l > max_l {
                    max_l = sum_l;
                    max_gp = gp;
                }
                sum_l = Likelyhood::zero();
            } else {
                sum_l = sum_l + l;
            }
        }
        (max_gp, max_l)
    }

    pub fn write_results_vcf<P: AsRef<Path>>(&self, path: P, gzip: bool) -> Result<()> {
        let results: Vec<_> = (if self.cfg.use_state_likelihood {
                debug!("Selecting candidates of best states");
                self.best_states()
            } else {
                debug!("Selecting best candidates");
                self.best_candidates()
            }).iter().map(|st| (*st).cand.clone() ).collect();
        write_candidates_vcf(path, &self.sampleset, &results[..], &self.unphased, gzip)
    }

    ///
    fn merge_prefixed_states(&self, states: &[State], prefix_lens: [usize; 2]) -> State {
        let cand_tuples: Vec<(&GenoPair, Likelyhood)> = states.iter().map(|s| (&s.cand, s.cand_l)).collect();
        let (ncand_gp, ncand_l) = Self::merge_candidates(cand_tuples);
        let s0 = &states[0];
        State {
            hist: GenoPair::from_chains(map01(|i| {
                let l = s0.hist.seqs[i].len();
                s0.hist.seqs[i].slice(l - prefix_lens[i] .. l) })),
            refs: map01(|i| self.pbwt.match_start_range(ncand_gp.seqs[0].len(), s0.refs[i].range.start)),
            state_l: states.iter().map(|s| s.state_l).sum::<Likelyhood>(),
            cand_l: ncand_l,
            cand: ncand_gp.clone(),
        }
    }

    /// Merge states that have likelihood under cfg.state_merge_fraction.
    /// Returns `(the number of states merged, fraction of likelihood affected)`.
    pub fn merge_unlikely_states(&self, states: &mut Vec<State>) -> (usize, f64) {
        match self.cfg.state_merge_fraction {
            Some(smf) => {
                let sum_likelihood = states.iter().map(|s| s.state_l).sum::<Likelyhood>();
                let mut old_states = Vec::new();
                mem::swap(states, &mut old_states);
                let (ms, mll) = self.merge_unlikely_states_rec(old_states, sum_likelihood * smf,
                    states, 0, [0, 0]);
                (ms, (mll / sum_likelihood).to_float())
            }
            None => (0, 0.0)
        }
    }

    fn merge_unlikely_states_rec(&self, states: Vec<State>, limit_ll: Likelyhood, dest: &mut Vec<State>, branch_on: usize,
        prefix_lens: [usize; 2]) -> (usize, Likelyhood) {
        if states.len() <= 1 {
            dest.extend(states);
            return (0, Likelyhood::zero());
        }
        let sum_likelihood = states.iter().map(|s| s.state_l).sum::<Likelyhood>();
        if sum_likelihood <= limit_ll {
            dest.push(self.merge_prefixed_states(&states, prefix_lens));
            return (states.len(), sum_likelihood);
        }
        // Continuing with 0, 1, or *
        let (mut ss0, mut ss1, mut ss2) = (Vec::new(), Vec::new(), Vec::new());
        for s in states {
            let n = {
                let hist = &s.hist.seqs[branch_on];
                if hist.len() > prefix_lens[branch_on] {
                    hist.get_end(prefix_lens[branch_on]) as usize
                } else {
                    2
                }};
            [&mut ss0, &mut ss1, &mut ss2][n].push(s);
        }
        let mut pl2 = prefix_lens;
        let mut tot_num = 0;
        let mut tot_ll = Likelyhood::zero();
        pl2[branch_on] += 1;
        if !ss0.is_empty() {
            let (num, ll) = self.merge_unlikely_states_rec(ss0, limit_ll, dest, 1 - branch_on, pl2);
            tot_num += num;
            tot_ll = tot_ll + ll;
        }
        if !ss1.is_empty() {
            let (num, ll) = self.merge_unlikely_states_rec(ss1, limit_ll, dest, 1 - branch_on, pl2);
            tot_num += num;
            tot_ll = tot_ll + ll;
        }
        if !ss2.is_empty() {
            let (num, ll) = if prefix_lens[1 - branch_on] < prefix_lens[branch_on] - 2 {
                let sum_likelihood2 = ss2.iter().map(|s| s.state_l).sum::<Likelyhood>();
                dest.push(self.merge_prefixed_states(&ss2, prefix_lens));
                (ss2.len(), sum_likelihood2)
            } else {
                self.merge_unlikely_states_rec(ss2, limit_ll, dest, 1 - branch_on, prefix_lens)
            };
            tot_num += num;
            tot_ll = tot_ll + ll;
        }
        (tot_num, tot_ll)
    }

}
 

#[cfg(test)]
mod tests {
    use super::*;
}
