use pbwt::{PBWTCursor, PBWTState};
use super::super::*;
use crate::utils::*;
use std::cmp::max;

#[derive(Debug, PartialEq)]//, Serialize, Deserialize)]
pub struct State {
    pub hist: GenoPair,
    pub refs: [PBWTCursor; 2],
    pub cand: GenoPair,
    pub state_l: Likelyhood,
    pub cand_l: Likelyhood,
}

impl State {
    pub fn new(refs: PBWTCursor) -> Self {
        State {
            hist: GenoPair::new(),
            refs: [refs.clone(), refs],
            state_l: Likelyhood::from_float(1.0),
            cand_l: Likelyhood::from_float(1.0),
            cand: GenoPair::new(),
        }
    }

    /// Create and push `Sate` extensions compatible with unphased `vp`.
    pub fn push_extension_with(&self, res: &mut Vec<Self>, index: &PBWTState, vp: VariantPair) {
        if vp[0] == vp[1] {
            match self.extend_val(index, vp) { Some(x) => res.push(x), _ => () };
        } else {
            match self.extend_val(index, [0, 1]) { Some(x) => res.push(x), _ => () };
            match self.extend_val(index, [1, 0]) { Some(x) => res.push(x), _ => () };
        }
    }

    /// Create and push `Sate` extensions with likelihoods pl
    pub fn push_extension_pl(&self, res: &mut Vec<Self>, index: &PBWTState, pl: &[Likelyhood]) {
        match self.extend_val_pl(index, [0, 0], pl[0]) { Some(x) => res.push(x), _ => () };
        match self.extend_val_pl(index, [1, 0], pl[1]) { Some(x) => res.push(x), _ => () };
        match self.extend_val_pl(index, [0, 1], pl[1]) { Some(x) => res.push(x), _ => () };
        match self.extend_val_pl(index, [1, 1], pl[2]) { Some(x) => res.push(x), _ => () };
    }

    pub fn extend_val(&self, index: &PBWTState, vp: VariantPair) -> Option<Self> {
        let refs = map01(|i| self.refs[i].extend_with(index, vp[i] != 0));
        if refs[0].len() == 0 || refs[1].len() == 0 { return None; }
        let mult = Likelyhood::from_float(
            ((refs[0].len() as f64) * (refs[1].len() as f64)) /
            ((self.refs[0].len() as f64) * (self.refs[1].len() as f64)));
        Some( State {
            hist: self.hist.extend(vp),
            refs: refs,
            state_l: self.state_l * mult,
            cand_l: self.cand_l * mult,
            cand: self.cand.extend(vp),
        })
    }

    /// extend_val with likelyhood rescaling
    fn extend_val_pl(&self, index: &PBWTState, vp: VariantPair, pl: Likelyhood) -> Option<Self> {
        let pl = max(pl, Likelyhood::from_float(super::computation::MIN_CROSSOVER_L));
        match self.extend_val(index, vp) {
            Some(mut s) => {
                s.cand_l = s.cand_l * pl;
                s.state_l = s.state_l * pl;
                Some(s) },
            None => None
        }
    }
}

impl Clone for State {
    fn clone(&self) -> Self {
        State {
            hist: self.hist.clone(),
            refs: map01(|i| self.refs[i].clone()),
            cand: self.cand.clone(),
            state_l: self.state_l,
            cand_l: self.cand_l,
        }
    }
}

