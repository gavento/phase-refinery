#![allow(unused_imports)]
#![feature(const_fn)]
#![feature(test)]

#[macro_use] extern crate quick_error;
#[macro_use] extern crate lazy_static;

use rust_htslib;
#[macro_use] extern crate itertools;
#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;


use rayon;
use pbwt;
use flate2;
use owning_ref;

#[cfg(test)]
extern crate test;

pub mod phasing;
pub use crate::phasing::{Computation, ComputationCfg, State};

pub mod lognum;
pub use crate::lognum::{LogFixPoint, LogFixPointType, LogF64};

pub mod utils;
pub use crate::utils::{GenoPair, GenoPairIter, WeightedStat, ChainBitVec, ChainBitVecIter, SimpleHash};

pub mod formats;
pub use crate::formats::{SampleSet, SampleSetRecord, GeneMap};

pub mod errors;
pub use crate::errors::*;

/// Type for likelyhood computation
pub type Likelyhood = lognum::LogFixPoint;

/// Type of location value in the genome and variant index
/// Type of sample (or reference) index in the reference panel
pub type Index = u32;

/// Genotype variant value
/// Non-negative values (0, 1, ...) are states, -1 a missing value
pub type Variant = i8;

/// Pair of variants in a location of a genotype
pub type VariantPair = [Variant; 2];
