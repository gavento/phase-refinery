#![allow(non_snake_case)]
//#![link_args = "-L /home/gavento/.local/lib"]

#[allow(unused_imports)]
use rayon::prelude::*;
use stopwatch::Stopwatch;
use clap::App;
use log::{info, log};

#[allow(unused_imports)]
use ::phasing::*;

/// Parse a list of type "1,3-6,8,9,10-20" into Vec<usize>
/// Any parse errors lead to a `panic!`
fn parse_range_list(s: &str) -> Vec<Index> {
    let mut r = Vec::new();
    for ss in s.split(",") {
        if let Some(cpos) = ss.find("-") {
            let (fs, ts) = ss.split_at(cpos);
            for i in fs.parse().expect("parsing range start") ..
                (ts[1..].parse::<Index>().expect("parsing range end") + 1) {
                r.push(i);
            }
        } else {
            r.push(ss.parse().expect("parsing list element"));
        }
    }
    r
}

use std::thread;

fn main() {
    let builder = thread::Builder::new()
                  .name("main".into())
                  .stack_size(128 * 1024 * 1024);
    let handler = builder.spawn(run).unwrap();
    handler.join().unwrap();
}

fn run() {

    let args = App::new("phase")
        .author("Tomas Gavenciak <gavento@ucw.cz>")
        .args_from_usage("
             -p, --panel <PANEL>   'Reference panel file name (VCF/BCF)'
             -i, --input <INPUT>   'Variation sample input file name (VCF/BCF)'
             -o, --output <OUTPUT> 'Output file name (.vcf or .vcf.gz)'
             -g, --gmap <GMAP>     'Genetic map file'
             -M, --markers [M]     'Limit on markers to read from the panel and samples\
            [default:all]'
             -N, --refs [N]        'Limit on references read from the panel [default: all]'
             -D, --drop-states [D] 'States kept after dropping least-likely [default: 1000]'
             -S, --samples [S]     'Numbers of variation samples to resolve (e.g. 1-2,6,8,20-23) [default: all]'
             -a, --IBD [a]         'Expected IBD length in centiMorgans (cM) [default: 1.0]'
             -s, --state-likelyhood 'Uses state likelihood instead of candidate likelyhood with -D, -P and output'
             -d, --debug           'Debug logging mode'
             -P, --progress        'Log progress with increasing intervals'
             -r, --refine          'Perform genotype refinement before phasing, expects PL in input [default: only phase]'
        ").get_matches();

    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{:5}][{:6}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S%.3f]"),
                record.level(),
                record.target(),
                message
            ))
        })
        .level(if args.is_present("debug") {
            log::LevelFilter::Debug } else { log::LevelFilter::Info })
        .chain(::std::io::stderr())
        .apply()
        .unwrap();


    let a_M: Option<Index> = args.value_of("markers").map(|s| s.parse().expect("parsing M"));
    let a_N: Option<Index> = args.value_of("refs").map(|s| s.parse().expect("parsing N"));
    let a_output = args.value_of("output").unwrap();
    let a_input = args.value_of("input").unwrap();
    let a_gmap = args.value_of("gmap").unwrap();
    let a_panel = args.value_of("panel").unwrap();
    let a_samples: Option<Vec<Index>> = args.value_of("samples").map(|s| parse_range_list(s));
    let a_state_l = args.is_present("state-likelyhood");
    let a_D = args.value_of("drop-states").unwrap_or("1000").parse().expect("parsing D");
    let a_IBD: f64 = args.value_of("IBD").unwrap_or("1.0").parse().expect("parsing IBD");
    let a_refine = args.is_present("refine");
    let a_progress = args.is_present("progress");

    let st1 = Stopwatch::start_new();
    let gm = GeneMap::read(&a_gmap).expect("reading gene map");
    info!("gene map {:?} read in {} ms, {} points", a_gmap, st1.elapsed_ms(), gm.locs.len());

    let st2 = Stopwatch::start_new();
    let input0 = SampleSet::read(&a_input, &a_samples, true).expect("reading input");
    info!("input samples {:?} read in {} ms, {} samples", a_input, st2.elapsed_ms(), input0.m());

    let st3 = Stopwatch::start_new();
    let refrange: Option<Vec<Index>> = a_N.map(|n| (0..n).collect());
    let panel0 = SampleSet::read(&a_panel, &refrange, false).expect("reading reference panel");
    info!("panel samples {:?} read in {} ms, {} haplotypes", a_panel, st3.elapsed_ms(), 2 * panel0.m());

    let cfg = ComputationCfg {
        limit_states: a_D,
        new_var_prob: Likelyhood::from_float(1e-6),
        mean_ibd: a_IBD * 0.01,
        use_state_likelihood: a_state_l,
        phase_only: !a_refine,
        state_merge_fraction: None, //Some(Likelyhood::from_float(1e-12)),
    };

    let mut c = Computation::new(panel0, input0, gm, cfg);
    let st = Stopwatch::start_new();
    let mut num: Index = 0;
    let mut progress_dt = 2000;
    while c.step().unwrap() {
        num += 1;
        if a_M.is_some() && a_M.clone().unwrap() <= num {
            break;
        }
        let dt = st.elapsed_ms();
        if dt >= progress_dt && a_progress {
            progress_dt = progress_dt * 3 / 2;
            let speed = 1000.0 * (num as f64) / (dt as f64);
            info!("Processed {} variants in {:.2} s ({:.2} steps/s, {} s remaining)",
                num, dt as f64 / 1000.0, speed, match a_M {
                    Some(m) => format!("{:.2}", (m - num) as f64 / speed),
                    None => "?".into(),
                });
        }
    }
    let dt = st.elapsed_ms();
    info!("Done: ran {} steps in {} ms ({} steps/s).",
        num, dt, 1000.0 * (num as f64) / (dt as f64));

    let st5 = Stopwatch::start_new();
    assert!(a_output.ends_with(".vcf") || a_output.ends_with(".vcf.gz"));
    c.write_results_vcf(&a_output, a_output.ends_with(".vcf.gz")).expect("writing best candidates");
    info!("written best candidates to {:?} in {} ms", a_output, st5.elapsed_ms());
}
