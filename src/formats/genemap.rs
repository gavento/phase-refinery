use super::super::*;
use std::io::{BufReader, BufRead, Write, BufWriter};
use std::fs::File;
use std::path::Path;
use std::str::FromStr;

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct GeneMap {
    pub name: String,
    pub locs: Vec<Index>,
    /// Morgans / bp
    pub rate: Vec<f64>,
    /// Morgans
    pub gmap: Vec<f64>,
}

impl GeneMap {
    pub fn read<P: AsRef<Path>>(path: &P) -> Result<Self> {
        let mut lines = BufReader::new(File::open(path)?).lines();
        let header = lines.next().some_or("Empty genemap file")?;
        if header != "position COMBINED_rate(cM/Mb) Genetic_Map(cM)" {
            return Err(format!("Unexpected header: {:?}", header).into());
        }
        let mut gm = GeneMap {
            name: path.as_ref().to_string_lossy().into_owned(),
            ..Default::default()
        };
        for lr in lines {
            let lres = lr?;
            let mut l = lres.split_whitespace();
            gm.locs
                .push(Index::from_str(l.next().ok_or("Missing field 0")?)
                          .box_err()?);
            gm.rate
                .push(1e-8 * f64::from_str(l.next().ok_or("Missing field 1")?).box_err()?);
            gm.gmap
                .push(1e-2 * f64::from_str(l.next().ok_or("Missing field 2")?).box_err()?);
        }
        Ok(gm)
    }

    /// Return the interpolated rate in Morgans / bp
    pub fn get_rate(&self, loc: Index) -> f64 {
        utils::interpolate(loc, &self.locs, &self.rate)
    }

    /// Return the interpolated position in Morgans
    pub fn get_gmap(&self, loc: Index) -> f64 {
        utils::interpolate(loc, &self.locs, &self.gmap)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_gmap() {
        let gm = GeneMap::read(&"../ex-chr20/gmap_chr20.txt").unwrap();
        assert!((gm.get_rate(1) - 0.3516e-8).abs() < 1e-3);
        assert!((gm.get_rate(100000) - 2.4694e-8).abs() < 1e-3);
        assert!((gm.get_rate(83570) - 0.5901e-8).abs() < 1e-3);
        assert!((gm.get_gmap(83570) - 0.0827e-2).abs() < 1e-3);
        assert!((gm.get_rate(72000) - 8.2402e-8).abs() < 1e-3);
    }
}
