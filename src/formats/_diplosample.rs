#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct VariantSample {
    pub sample_name: String,
    pub positions: Vec<Position>,
    pub probs: Vec<Vec<Likelyhood>>,
    pub alleles: Vec<Vec<String>>,
}

impl VariantSample {
    pub fn read_many<P: AsRef<Path>>(path: &P,
                                samples_opt: Option<Vec<usize>>,
                                num_locs_: Option<i64>)
                                -> Result<Vec<Self>> {
        let r = bcf::Reader::from_path(path).box_err()?;
        let samples: Vec<_> = samples_opt.unwrap_or((0 .. r.header.sample_count() as usize).collect());
        if samples.iter().any(|snum| *snum >= r.header.sample_count() as usize) {
            Err("sample number too high")?
        }
        let mut rem_locs = num_locs_.unwrap_or(-1);
        let mut vss: Vec<_> = samples.iter().map(|snum| VariantSample {
            sample_name: String::from_utf8_lossy(r.header.samples()[*snum]).into_owned(),
            ..Default::default() }
        ).collect();

        for rec_res in r.records() {
            if rem_locs == 0 {
                break;
            }
            rem_locs -= 1;
            let mut rec = rec_res.box_err()?;
            if rec.allele_count() > 2 { continue; } // NOTE: Biallelic locations only
            for (snum, mut vs) in samples.iter().zip(&mut vss) {
                vs.positions.push(rec.pos() as LocIndex);
                vs.alleles.push(rec.alleles().iter()
                                   .map(|sl| String::from_utf8_lossy(*sl).into_owned())
                                   .collect());
                let fmt = rec.format("PL".as_bytes()).integer().box_err()?[*snum];
                let probs: Vec<f64> = fmt.iter().map(|x| 0.7943282347242815f64.powi(*x)).collect();
                let sum: f64 = probs.iter().sum();
                vs.probs
                    .push(probs
                        .iter()
                        .map(|p| Likelyhood::from_float(p / sum))
                        .collect());
            }
        }
        Ok(vss)
    }

    pub fn filter_positions(self, pos: &[Position]) -> Self {
        let mut positions = Vec::new();
        let mut probs = Vec::new();
        let mut alleles = Vec::new();
        for (pos, (_pos_1, (prob, als)), _pos_2) in filter_common_by(
            self.positions.into_iter().zip(self.probs.into_iter().zip(self.alleles)), |pp| pp.0,
            pos.iter(), |p| **p) {
            positions.push(pos);
            probs.push(prob);
            alleles.push(als);
        }
        VariantSample {
            sample_name: self.sample_name.clone(),
            positions: positions,
            probs: probs,
            alleles: alleles,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_varsample() {
        let samples = Some(vec![3]);
        let vs = VariantSample::read_many(&"testdata/chr20.bcf", samples,
                                          Some(32)).unwrap()[0].clone();
        assert_eq!(vs.positions.len(), 32);
        assert_eq!(vs.probs.len(), 32);
        assert_eq!(vs.probs[0].len(), 3);
        assert!((vs.probs[0].iter().map(|x| x.to_float()).sum::<f64>() - 1.0).abs() < 1e-6);
        println!("{:?}", &vs);
    }
}
